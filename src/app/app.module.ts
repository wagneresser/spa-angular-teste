import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { PublicModule } from './public/public.module';
import { AppRoutingModule } from './app-routing.module';
import { PokemonListResolver } from './public/pokemon-list/pokemon-list.resolver';
import { PokemonDetailResolver } from './public/pokemon-detail/pokemon-detail.resolver';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    PublicModule,
    AppRoutingModule,

    // bootstrap
    // ...
  ],
  providers: [PokemonListResolver, PokemonDetailResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
