export interface PokemonOptions {
    id: number;
    name: string;
    weight: string;
    height: string;
    base_experience: number;
}
