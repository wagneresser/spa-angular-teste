import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './public/home/home.component';
import { PokemonDetailComponent } from './public/pokemon-detail/pokemon-detail.component';
import { PageNotFoundComponent } from './public/page-not-found/page-not-found.component';
import { PokemonListComponent } from './public/pokemon-list/pokemon-list.component';
import { PokemonListResolver } from './public/pokemon-list/pokemon-list.resolver';
import { ContactComponent } from './public/contact/contact.component';
import { PokemonDetailResolver } from './public/pokemon-detail/pokemon-detail.resolver';


const routes: Routes = [
  { path: '404', component: PageNotFoundComponent },
  {
    path: 'home',
    component: HomeComponent,
    resolve: {
      pokemonList: PokemonListResolver
    },
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'contact', component: ContactComponent },
  {
    path: 'pokemon',
    children: [
      {
        path: 'list',
        component: PokemonListComponent,
        resolve: {
          pokemonList: PokemonListResolver
        }
      },
      {
        path: ':id',
        component: PokemonDetailComponent,
        resolve: {
          pokemon: PokemonDetailResolver
        }
      }
    ]
  },
  { path: '**', redirectTo: '404' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
